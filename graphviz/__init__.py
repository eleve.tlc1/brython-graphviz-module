# This is the graphviz package by Sebastian Bank, stripped down and adapted to Brython by
# Christophe Perrod under the GNU GPL v3 License.

# graphviz - init module

######
# Added for Brython support -- start

from browser import load, window

load('graphviz/viz.min.js')
load('graphviz/full.render.min.js')


# Added for Brython support -- end
######

from .dot import Graph, Digraph



__title__ = 'graphviz'
__version__ = '0.15.dev0'
__author__ = 'Sebastian Bank <sebastian.bank@uni-leipzig.de>'
__license__ = 'MIT, see LICENSE.txt'
__copyright__ = 'Copyright (c) 2013-2020 Sebastian Bank'



ENGINES = {  # http://www.graphviz.org/pdf/dot.1.pdf
    'dot', 'neato', 'twopi', 'circo', 'fdp', 'sfdp', 'patchwork', 'osage',
}



class ExecutableNotFound(RuntimeError):
    """Exception raised if the Graphviz executable is not found."""

    _msg = ('failed to execute %r, '
            'make sure the Graphviz executables are on your systems\' PATH')

    def __init__(self, args):
        super(ExecutableNotFound, self).__init__(self._msg % args)


class RequiredArgumentError(Exception):
    """Exception raised if a required argument is missing."""



#: Set of known layout commands used for rendering (``'dot'``, ``'neato'``, ...)
ENGINES = ENGINES

ExecutableNotFound = ExecutableNotFound

RequiredArgumentError = RequiredArgumentError
